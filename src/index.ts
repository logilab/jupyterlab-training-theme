import {
  JupyterFrontEnd,
  JupyterFrontEndPlugin
} from '@jupyterlab/application';

import { IThemeManager } from '@jupyterlab/apputils';

/**
 * Initialization data for the trainingTheme extension.
 */
const plugin: JupyterFrontEndPlugin<void> = {
  id: 'trainingTheme:plugin',
  description: 'Theme for jupyterlab-training',
  autoStart: true,
  requires: [IThemeManager],
  activate: (app: JupyterFrontEnd, manager: IThemeManager) => {
    console.log('JupyterLab extension training-theme is activated!');
    const style = 'trainingTheme/index.css';

    manager.register({
      name: 'trainingTheme',
      isLight: true,
      load: () => manager.loadCSS(style),
      unload: () => Promise.resolve(undefined)
    });
  }
};

export default plugin;
